# Treasure Hunt
Treasure Hunt is a fun and exciting game where you search for hidden treasures while avoiding dangers like traps, monsters, and more! Follow the instructions below to give it a try yourself!

This game is intended for Android users.

Repository for MA/SE&amp;WM - Group Afternoon 7

## Environment Setup
- Install [Lombok](https://projectlombok.org/setup/android) plugin for Android Studio
    Annotation Processing 
   - set the compileOnly attribute
   ```
    dependencies {
	    compileOnly 'org.projectlombok:lombok:1.18.26'
	    annotationProcessor 'org.projectlombok:lombok:1.18.26'
    }
   ```
- Calculate task graph
   - Uncheck File -> Settings -> Build/Execution/Deployment -> Compiler -> Configure On Demand

## Build Status
<a href="http://dhdm.de:8111/viewType.html?buildTypeId=Geoschnitzel_Build&guest=1">
<img src="http://dhdm.de:8111/app/rest/builds/buildType:(id:Geoschnitzel_Build)/statusIcon"/>
</a>

## Contributing
Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

You can browse the code, issues, and more at TreasureHunt's [GitLab](https://gitlab.com/GeoSchnitzel/TreasureHunt/?_gl=1%2a59cchi%2a_ga%2aMzA3ODUwMjM5LjE2NzU5NjQzODg.%2a_ga_ENFH3X7M5Y%2aMTY3NTk2NDM4OC4xLjEuMTY3NTk2NDg4Ny4wLjAuMA) repository.

## Licence
[GNU Affero General Public License v3.0](https://choosealicense.com/licenses/agpl-3.0/)